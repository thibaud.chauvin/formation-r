# Formation R

Répertoire pour trouver les documents de la session 1 & 2 de la Formation R - les bases

Vous y trouverez : 

- les pdf de chaque session : Formation_R_Session_1.pdf & Formation_R_Session_2.pdf

- le script des exercices avec les solutions pour les 2 sessions  

- un pdf RGuideLinesFr.pdf pour avoir les conventions de codage 

- un pdf Rcolor présentant toutes les couleurs de base dans R (#paillettesdanslesyeux)

Enjoy 
